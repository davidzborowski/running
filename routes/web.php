<?php

Route::get('/test',function(){
    $runs = [];
    $prev = 0;
    for($i = 0; $i<date('z');$i++){
        $date = date('Y-m-d',strtotime('1st January 2018 + ' . $i . ' days'));
        $distance = \App\Run::whereDate('start_date',$date)->get()->sum('distance')/1000 ?? 0;
        $distance += $prev;
        $runs[$date] = $distance;
        $prev = $distance;
    }
    $points = \Illuminate\Support\Facades\Cache::get('points');
    $points_count = count($points);
    foreach($runs as $date => $distance) {
        $current_distance = $distance;
        $calculated_distance = 0;
        $count = 1;
        $last_point = $points[0];
        for ($i = 1; $i < $points_count; $i++) {
            if ($calculated_distance < $current_distance) {
                $count++;
                $current_point = $points[$i];
                $distance = distance($last_point[0], $last_point[1], $current_point[0], $current_point[1], 'km', 10);
                if (!is_nan($distance)) {
                    $calculated_distance += $distance;
                    $last_point = $current_point;
                }

            }

        }
        $runs[$date] = getTown(implode(',',$points[$count]));
    }
    dd($runs);


});

Route::get('/',function(){

    $points = \Illuminate\Support\Facades\Cache::get('points');
    $points_count = count($points);
    $current_distance = distanceRan();
    $calculated_distance = 0;
    $count = 1;
    $last_point = $points[0];
    for($i=1;$i<$points_count;$i++){
        if($calculated_distance<$current_distance) {
            $count++;
            $current_point = $points[$i];
            $distance = distance($last_point[0], $last_point[1], $current_point[0], $current_point[1],'km',10);
            if(!is_nan($distance)){
                $calculated_distance += $distance;
                $last_point = $current_point;
            }

        }

    }
    $done_points = $points->take($count);
    $current_point = $points->slice($count-1,1)->first();
    $todo_points = $points->slice($count-1,10000);
    $todo_points2 = $points->slice($count-1+10000,$points_count-$count-10000);
    $i = 0;
    $result = [];
    foreach ($todo_points2 as $point){
        if ($i++ % 5 == 0) {
            $result[] = $point;
        }
    }
    $todo_points2 = collect($result);

    $todo_points = $todo_points->merge($todo_points2);
    $last_sunday = date('Y-m-d',strtotime('last sunday'));
    $last_monday = date('Y-m-d',strtotime($last_sunday . ' - 6 days'));
    $last_week_runs = round(\App\Run::whereDate('start_date','>=',$last_monday)->whereDate('start_date','<=',$last_sunday)->sum('distance')/1000,3);
    $yesterday_distance = round(App\Run::whereDate('start_date',date('Y-m-d',strtotime('yesterday')))->sum('distance')/1000,3);
    return view('homepage')->with([
        'todays' => \App\Run::with('athlete')->whereDate('start_date',date('Y-m-d',strtotime('today')))->get()->sortByDesc('start_date'),
        'yesterday_distance' => $yesterday_distance,
        'last_week_distance' => $last_week_runs,
        'runs' => \App\Run::with('athlete')->get()->groupBy(function($item){
            return date('Y-m',strtotime($item->start_date));
        })->sortByDesc(function($item,$key){
            return $key;
        }),
        'athletes' => \App\Athlete::all(),
        'points' => [$done_points,$todo_points,$current_point],
        'current_town' => getTown(implode(',',$points[$count])),
    ]);

});

Route::get('/manual',function(){
    return view('manual');
});

Route::post('/manual',function(\Illuminate\Http\Request $request){
    if($request->password == env('ADMIN_PASS')) {
        $run = new \App\Run();
        $run->external_id = "manual_" . time();
        $run->athlete_id = $request->athlete;
        $run->name = $request->title;
        $run->distance = floatval($request->distance * 1000);
        $run->start_date = date('Y-m-d H:i:s', strtotime($request->start_time));
        $run->save();
    }
    return redirect('/');
});

Route::post('/manual_remove',function(\Illuminate\Http\Request $request){
    if($request->password == env('ADMIN_PASS')) {
        \App\Run::find($request->run)->delete();
    }
    return redirect('/');
});

