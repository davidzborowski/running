<?php

namespace App\Console\Commands;

use App\Point;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class GetPoints extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:points';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        Cache::flush();
        $points = \Illuminate\Support\Facades\Cache::rememberForever('points', function() {
            $all_points = [];
            if (($handle = fopen(storage_path('points.csv'), 'r')) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
                    $all_points[] = $data;
                }
                fclose($handle);
            }
            return collect($all_points);

        });
    }
}
