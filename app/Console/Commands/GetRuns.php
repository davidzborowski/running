<?php

namespace App\Console\Commands;

use App\Athlete;
use App\Run;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class GetRuns extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:runs {--refresh}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Runs from Strava';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function handle(){
        $adapter = new \Pest('https://www.strava.com/api/v3');
        $service = new \Strava\API\Service\REST(env('API_KEY2'), $adapter);  // Define your user token here.
        $client = new \Strava\API\Client($service);
        $activities = $client->getClubActivities(336964,1,6);
        foreach($activities as $activity){
            $athlete = Athlete::all()->filter(function($item) use ($activity){
                $initial = strtolower(substr($activity['athlete']['lastname'],0,1));
                $name = strtolower($activity['athlete']['firstname']);
                if($name == strtolower($item->name) && starts_with(strtolower($item->surname),$initial)){
                    return true;
                }
                return false;
            })->first();
            if($athlete){
                $id = $activity;
                unset($id['name']);
                unset($id['workout_type']);
                $id = md5(serialize($id));
                $run = Run::where('external_id',$id)->first();
                if(is_null($run)){
                    $run = new Run();
                    $run->external_id = $id;
                    $run->athlete_id = $athlete->id;
                    $run->distance = $activity['distance'];
                    $run->start_date = date('Y-m-d H:i:s',time() - $activity['elapsed_time']);
                    $run->name = $activity['name'];
                    $run->save();
                }


            }
        }
    }

    public function handle_old()
    {
        $adapter = new \Pest('https://www.strava.com/api/v3');
        $service = new \Strava\API\Service\REST(env('API_KEY2'), $adapter);  // Define your user token here.
        $client = new \Strava\API\Client($service);
        dd($client->getAthleteActivities('2018-04-01','2018-03-01',1,20));

        if($this->option('refresh')){
            DB::table('athletes')->truncate();
            DB::table('runs')->truncate();
            $activities = $client->getClubActivities(336964,1,200);
            $activities2 = $client->getClubActivities(336964,2,200);
            $activities = array_merge($activities,$activities2);

        }else{
            $activities = $client->getClubActivities(336964,1,5);
        }
        foreach ($activities as $activity) {
            $athlete = Athlete::where('strava_id',$activity['athlete']['id'])->first();
            if(is_null($athlete)){
                $athlete = new Athlete();
                $athlete->strava_id = $activity['athlete']['id'];
                $athlete->name = $activity['athlete']['firstname'];
                $athlete->surname = $activity['athlete']['lastname'];
                $athlete->profile = $activity['athlete']['profile'];
                $athlete->save();
            }
            if (strtotime($activity['start_date']) > strtotime('1st January 2018')) {
                $run = Run::where('external_id',$activity['id'])->first();
                if(is_null($run)){
                    $run = new Run();
                    $run->external_id = $activity['id'];
                    $run->athlete_id = $athlete->id;
                    $run->distance = $activity['distance'];
                    $run->start_date = date('Y-m-d H:i:s',strtotime($activity['start_date']));
                    $run->name = $activity['name'];
                    $run->save();
                }
            }
        }
    }
}
