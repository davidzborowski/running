<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class Debug extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'debug';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dd(date('W'));
        $points = Cache::get('points');
        $bar = $this->output->createProgressBar(count($points));

        $f = fopen(storage_path('temp.csv'), 'w');
        foreach ($points as $line) {
            fputcsv($f, $line);
            $bar->advance();
        }
        fclose($f);
        $bar->finish();
    }

}
