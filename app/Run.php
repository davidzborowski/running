<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Run extends Model
{
    public function athlete(){
        return $this->belongsTo('App\Athlete');
    }

    public function getDistanceKmAttribute(){
        return $this->distance / 1000;
    }
}
