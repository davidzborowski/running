<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Athlete extends Model
{
    public function runs()
    {
        return $this->hasMany('App\Run');
    }

    public function getFormattedNameAttribute()
    {
        return ucwords($this->name . ' ' . $this->surname);
    }

    public function getTotalDistanceAttribute(){
        return round($this->runs()->sum('distance')/1000,3);
    }

    public function getProjectedDistanceAttribute(){
        $divide = (date('W')-1) + 52;
        if($divide < 1){
            $divide = 1;
        }
        $calculation = ((($this->total_distance/$divide)*52)/365)*(howManyDaysToGoal()+365);
        return round($calculation,3);
    }

    public function getWeeklyTargetAttribute(){
        $ability = ($this->total_distance / distanceRan());
        $distance = distanceToGoal() * $ability;
        $distancePerDay = $distance / howManyDaysToGoal();
        $distancePerWeek = $distancePerDay * 7;
        return round($distancePerWeek,3);
    }

    public function getWeeklyDistanceAttribute(){
        return round($this->runs->filter(function($run){
            return ((date('W')) == date('W',strtotime($run->start_date)) && date('Y',strtotime($run->start_date)) == '2019');
        })->sum('distance_km'),3);
    }

    public function getLeftWeeklyAttribute(){
        return $this->weekly_target-$this->weekly_distance;
    }

    public function getProfileAttribute(){
        $profile = $this->attributes['profile'];
        if(starts_with($profile,'http')){
            return $profile;
        }

        return "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png";
    }
}
