<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 06/02/18
 * Time: 14:16
 */

function distanceRan()
{
    return \Illuminate\Support\Facades\Cache::remember('distance_ran',1,function(){
        return round(\App\Run::all()->sum('distance_km'), 3);
    });
}

function distanceLastWeek()
{
    return \Illuminate\Support\Facades\Cache::remember('distance_ran_last_week',1,function(){

        return round(\App\Run::all()->filter(function($run){
            return (date('W')-1) == date('W',strtotime($run->start_date));
        })->sum('distance_km'),3);
    });
}
function distanceThisWeek()
{
    return \Illuminate\Support\Facades\Cache::remember('distance_ran_this_week',1,function(){
        return round(\App\Run::all()->filter(function($run){
            return (date('W')) == date('W',strtotime($run->start_date));
        })->sum('distance_km'),3);
    });
}

function distanceToGoal()
{
    return round(env('TOTAL_DISTANCE') - distanceRan(), 3);
}

function distanceAverage()
{
    return round(distanceRan() / (date('z')+365), 3);
}

function distancePercentage(){
    return round((distanceRan()/env('TOTAL_DISTANCE'))*100,0);
}
function distancePercentage2(){
    return round((distanceRan()/env('TOTAL_DISTANCE'))*100,1);
}

function howManyDaysToGoal(){
    return round(distanceToGoal()/distanceAverage(),0);
}

function whatDateWeWillReachGoal(){
    return date('d/m/Y',strtotime('today + ' . howManyDaysToGoal() . ' days'));
}

function daysInAMonth($date){
    $date = date('Y-m',strtotime($date));
    $current_month = date('Y-m');
    if($date != $current_month){
        return cal_days_in_month(CAL_GREGORIAN,date('m',strtotime($date)),date('Y',strtotime($date)));
    }else{
        return date('j');
    }
}

function howMuchWeNeedPerDay(){
    $calc = distanceToGoal() / howManyDaysToGoal();
    return round($calc,2);
}

function howMuchMoreWeNeedToday($today){
    $people = \App\Athlete::count() - $today->unique('athlete_id')->count();
    $distance = round(howMuchWeNeedPerDay() - round($today->sum('distance')/1000,2),2);
    if($distance > 0){
        $average = round($distance / $people,2);
        return "we need <strong>" . $distance . "km</strong> more to hit the target<br/><small>(That's only <strong>" . $average . "km</strong> per person)</small>";
    }else{
        return "Awesome - we've smashed our daily target! We have done <strong>" . ($distance*-1) . "km</strong> extra! Well Done!";
    }
}


function todaysPercentage($today){
    $daily = howMuchWeNeedPerDay();
    $distance = round($today->sum('distance')/1000,2);
    $percentage = round(($distance / $daily)*100,0);
//    $daily = 10.0;
//    $distance = 15.0;
//    $percentage = 150;
    $percentage_reverse = round(($daily / $distance)*100,0);
    $progress = '<div class="progress">
        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="'.$percentage.'" aria-valuemin="0" aria-valuemax="100" style="width: '.$percentage.'%;">
            '.$percentage.'%
        </div>
    </div>';
    if($percentage>100) {
        $p1 = $percentage_reverse;
        $p2 = 100 - $percentage_reverse;
        $progress = '<div class="progress">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="' . $p1 . '" aria-valuemin="0" aria-valuemax="100" style="width: ' . $p1 . '%;">
                100%
            </div>
            <div class="progress-bar progress-bar-warning" style="width: '.$p2.'%">
            '.($percentage-100).'% BONUS!
            </div>
        </div>';
    }

    return $progress;
}

function distance($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 4) {
    // Calculate the distance in degrees
    $point1_lat = floatval($point1_lat);
    $point1_long = floatval($point1_long);
    $point2_lat = floatval($point2_lat);
    $point2_long = floatval($point2_long);
    $degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));

    // Convert the distance in degrees to the chosen unit (kilometres, miles or nautical miles)
    switch($unit) {
        case 'km':
            $distance = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)
            break;
        case 'mi':
            $distance = $degrees * 69.05482; // 1 degree = 69.05482 miles, based on the average diameter of the Earth (7,913.1 miles)
            break;
        case 'nmi':
            $distance =  $degrees * 59.97662; // 1 degree = 59.97662 nautic miles, based on the average diameter of the Earth (6,876.3 nautical miles)
    }
    return round($distance, $decimals);
}
function getTown($current){
    $url="https://maps.googleapis.com/maps/api/geocode/json?address=".$current."&key=".env('GOOGLE_API');
    $data = json_decode(file_get_contents($url),true)['results'][0]['address_components'] ?? null;
    $town = '';
    if(!is_null($data)) {
        foreach ($data as $possible) {
            if (in_array('postal_town', $possible['types'])) {
                $town = $possible['long_name'];
            }
        }
        if (empty($town)) {
            foreach ($data as $possible) {
                if (in_array('locality', $possible['types'])) {
                    $town = $possible['long_name'];
                }
            }
        }
        foreach ($data as $possible) {
            if (in_array('country', $possible['types'])) {
                $town .= ', ' . $possible['long_name'];
            }
        }
    }
    return $town;
}


function imageForLocation($point,$prev_point){
    $heading = getRhumbLineBearing($point[0],$point[1],$prev_point[0],$prev_point[1]);
    return "https://maps.googleapis.com/maps/api/streetview?size=640x400&location=".implode(',',$point)."&fov=120&heading=".$heading."&pitch=10&key=".env('GOOGLE_API');
}

function getRhumbLineBearing($lat1, $lon1, $lat2, $lon2) {
    //difference in longitudinal coordinates
    $dLon = deg2rad($lon2) - deg2rad($lon1);

    //difference in the phi of latitudinal coordinates
    $dPhi = log(tan(deg2rad($lat2) / 2 + pi() / 4) / tan(deg2rad($lat1) / 2 + pi() / 4));

    //we need to recalculate $dLon if it is greater than pi
    if(abs($dLon) > pi()) {
        if($dLon > 0) {
            $dLon = (2 * pi() - $dLon) * -1;
        }
        else {
            $dLon = 2 * pi() + $dLon;
        }
    }
    //return the angle, normalized
    return (rad2deg(atan2($dLon, $dPhi)) + 360) % 360;
}