<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>EdRob Running</title>
    <link href="/css/app.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Inconsolata" rel="stylesheet">
    <style>
        .clock{


            font-family: 'Inconsolata', monospace;
            font-size: 44px;


        }
        body{
            padding-top:40px;
        }
        #map {
            height: 500px;
            max-width: 100%;
        }
        .panel-body{
            display:flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            min-height:120px;
        }
        .panel-title{
            text-align: center;
        }
        .panel-flex{
            display: flex;
            align-items: center;
            justify-content: space-between;
            flex-direction: row;
            min-height: 50px;
            width:100%;
        }
        .panel-flex h4{
            width:100%;
        }
    </style>
</head>
<body>
<div class="container">
</div>
<div class="container">
    <div class="row">
        <div class="column col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Total Distance</h3>
                </div>
                <div class="panel-body">
                    <span><strong class="clock">{{env('TOTAL_DISTANCE')}}</strong>km</span>
                </div>
            </div>
        </div>
        <div class="column col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">We have run</h3>
                </div>
                <div class="panel-body">
                    <span><strong class="clock">{{distanceRan()}}</strong>km</span>
                </div>
            </div>
        </div>
        <div class="column col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Still to go</h3>
                </div>
                <div class="panel-body">
                    <span><strong class="clock">{{distanceToGoal()}}</strong>km</span>
                </div>
            </div>
        </div>
        <div class="column col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Average per day</h3>
                </div>
                <div class="panel-body">
                    <span><strong class="clock">{{distanceAverage()}}</strong>km</span>
                    <div><small style="text-align: center">We will reach destination on <strong>{{ whatDateWeWillReachGoal() }}</strong> ({{howManyDaysToGoal()}} days)</small></div>
                </div>
            </div>
        </div>

        <div class="column col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">We are in or near</h3>
                </div>
                <div class="panel-body">
                    <span><strong class="clock" style="font-size: 24px;">{{$current_town}}</strong></span>


                </div>
            </div>
        </div>
        <div class="column col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Overall Progress</h3>
                </div>
                <div class="panel-body">
                    <span><strong class="clock" style="font-size: 24px;">{{distancePercentage2()}}</strong>%</span>
                    <div class="progress" style="width: 100%">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{distancePercentage()}}" aria-valuemin="0" aria-valuemax="100" style="width: {{distancePercentage()}}%;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="column col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Last week we ran</h3>
                </div>
                <div class="panel-body">
                    <span><strong class="clock">{{distanceLastWeek()}}</strong>km</span>
                </div>
            </div>
        </div>
        <div class="column col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">&nbsp;</h3>
                </div>
                <div class="panel-body">
                    <span><strong class="clock">    <a href='http://www.justgiving.com/edrob-runners' title='JustGiving - Sponsor me now!' target='_blank'><img src='http://www.justgiving.com/App_Themes/JustGiving/images/badges/badge10.gif' width='270' height='50' alt='JustGiving - Sponsor me now!' /></a>
</strong></span>
                </div>
            </div>
        </div>
        <div class="column col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Yesterday we ran</h3>
                </div>
                <div class="panel-body">
                    <span><strong class="clock">{{$yesterday_distance}}</strong>km</span>
                </div>
            </div>
        </div>
    </div>


</div>
<div class="container">
    <h2 style="text-align: center;">Today's Runs</h2>

    <div class="row">
        <div class="column col-md-12">
            @if($todays->count() > 0)
                <h4 style="text-align: center;">Total Today: <strong>{{round($todays->sum('distance')/1000,1)}}km</strong> {!! howMuchMoreWeNeedToday($todays) !!}</h4>
                {!! todaysPercentage($todays) !!}
                <table class="table table-striped table-condensed" >
                    <tr>
                        <th>Name</th>
                        <th>Distance</th>
                        <th>Date and Time</th>
                    </tr>
                    @foreach($todays as $run)
                        <tr>
                            <td style="vertical-align: middle;"><img src="{{$run->athlete->profile}}" style="max-width:32px;border-radius: 4px;"/>  {{ $run->athlete->formatted_name }}</td>
                            <td style="vertical-align: middle;">{{ $run->distance_km }}km</td>
                            <td style="vertical-align: middle;">{{date('d/m/Y H:i:s',strtotime($run->start_date))}}</td>
                        </tr>
                    @endforeach
                </table>
            @else
                <div class="alert alert-warning" style="text-align: center">No runs today :(<br/>Today's Target is <strong>{{howMuchWeNeedPerDay()}}km</strong>. That's only <strong>{{ round(howMuchWeNeedPerDay()/\App\Athlete::count(),2) }}km</strong> per person!</div>
            @endif
        </div>
    </div>
</div>
<div class="container" style="min-height: 500px;margin-bottom:20px;">
    <div id="map" style="border-radius: 4px;"></div>
    <script>

        // This example creates a 2-pixel-wide red polyline showing the path of
        // the first trans-Pacific flight between Oakland, CA, and Brisbane,
        // Australia which was made by Charles Kingsford Smith.

        function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 9,
                center: {lat: {{$points[2][0]}}, lng: {{$points[2][1]}} },
                mapTypeId: 'terrain'
            });

            var currentPath = [
                @foreach($points[0] as $point)
                { lat: {{$point[0]}}, lng: {{$point[1]}} },
                @endforeach
            ];
            var planedPath = [
                @foreach($points[1] as $point)
                { lat: {{$point[0]}}, lng: {{$point[1]}} },
                @endforeach
            ];
            var aPath = new google.maps.Polyline({
                path: planedPath,
                geodesic: true,
                strokeColor: '#FF0000',
                strokeOpacity: 1.0,
                strokeWeight: 5
            });

            var bPath = new google.maps.Polyline({
                path: currentPath,
                geodesic: true,
                strokeColor: '#00FF00',
                strokeOpacity: 1.0,
                strokeWeight: 10
            });

            aPath.setMap(map);
            bPath.setMap(map);
        }
    </script>

</div>

    <div class="container">
        @include('users')
        @foreach($runs as $month => $data)
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="panel-flex"><h4>
                                <table class="table">
                                    <tr>
                                        <th style="border-top: 0px solid black; width:20%;"></th>
                                        <th class="hidden-xs" style="border-top: 0px solid black; width:20%;">Distance</th>
                                        <th class="hidden-xs" style="border-top: 0px solid black; width:20%;">Difference</th>
                                        <th class="hidden-xs" style="border-top: 0px solid black; width:20%;">Average</th>
                                        <th class="hidden-xs" style="border-top: 0px solid black; width:20%;"></th>
                                    </tr>
                                    <tr>
                                        <td style="border-top: 0px solid black; width:20%;">{{date('F Y',strtotime($month))}}</td>
                                        <td class="hidden-xs" style="border-top: 0px solid black;width:20%;">{{round($data->sum('distance')/1000,1)}}km</td>
                                        <td class="hidden-xs" style="border-top: 0px solid black;width:20%;">{!!( ((round($data->sum('distance')/1000 - env('TOTAL_DISTANCE')/12,1)) > 0)?'<span class="label label-success">+'.(round($data->sum('distance')/1000 - env('TOTAL_DISTANCE')/12,1)).'km</span>' : '<span class="label label-danger">'.(round($data->sum('distance')/1000 - env('TOTAL_DISTANCE')/12,1)).'km</span>')!!}</td>
                                        <td class="hidden-xs" style="border-top: 0px solid black;width:20%;">{{round(($data->sum('distance')/1000)/daysInAMonth($month),1)}}km</td>
                                        <td style="border-top: 0px solid black;width:20%;"><button class="btn btn-primary pull-right" type="button" data-toggle="collapse" data-target="#{{$month}}" aria-expanded="false" aria-controls="{{$month}}">Toggle</button></td>
                                    </tr>
                                </table>
                                 </h4></div>
                        <div class="collapse" id="{{$month}}" style="width: 100%;">
                            <div class="well">
                                <table class="table table-striped table-condensed" >
                                    <tr>
                                        <th>Name</th>
                                        <th>Distance</th>
                                        <th>Date and Time</th>
                                    </tr>
                                    @foreach($data->sortByDesc('start_date') as $run)
                                        <tr>
                                            <td style="vertical-align: middle;"><img src="{{$run->athlete->profile}}" style="max-width:32px;border-radius: 4px;"/>&nbsp;&nbsp;&nbsp;{{ $run->athlete->formatted_name }}</td>
                                            <td style="vertical-align: middle;">{{ $run->distance_km }}km</td>
                                            <td style="vertical-align: middle;">{{date('d/m/Y H:i:s',strtotime($run->start_date))}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

        @endforeach

    </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_API')}}&callback=initMap"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<p style="width:100%;padding:20px 0;text-align: center;">Developed by David Zborowski {{date('Y')}}</p>
<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
</body>
</html>
