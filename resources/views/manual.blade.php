<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>EdRob Running</title>
    <link href="/css/app.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Inconsolata" rel="stylesheet">
    <style>
        .clock{


            font-family: 'Inconsolata', monospace;
            font-size: 44px;


        }
        body{
            padding-top:40px;
        }
        #map {
            height: 500px;
            max-width: 100%;
        }
        .panel-body{
            display:flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            min-height:120px;
        }
        .panel-title{
            text-align: center;
        }
        .panel-flex{
            display: flex;
            align-items: center;
            justify-content: space-between;
            flex-direction: row;
            min-height: 50px;
            width:100%;
        }
        .panel-flex h4{
            width:100%;
        }
    </style>
</head>
<body>
<div class="container">
    <form action="/manual" method="post">
        {{csrf_field()}}
        <div class="form-group">
            <label for="exampleInputFile">Title</label>
            <input type="text" name="title" class="form-control" id="exampleInputPassword1" placeholder="Title">
        </div>
        <div class="form-group">
            <label for="exampleInputFile">Athlete</label>
            <select name="athlete" class="form-control">
                @foreach(\App\Athlete::all() as $athlete)
                    <option value="{{$athlete->id}}">{{$athlete->formatted_name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Distance (in KM)</label>
            <input type="number" name="distance" step="0.001" min="0.001" class="form-control" id="exampleInputEmail1" placeholder="0.001">
        </div>

        <div class="form-group">
            <label for="exampleInputFile">Run Start Date & Time</label>
            <input type="datetime-local" name="start_time" class="form-control" id="exampleInputPassword1" value="{{date('Y-m-d\TH:i:s')}}">
        </div>
        <div class="form-group">
            <label for="exampleInputFile">Password</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
        </div>

        <button type="submit" class="btn btn-success">Add</button>
    </form>

    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>

    <form action="/manual_remove" method="post">
        {{csrf_field()}}
        <div class="form-group">
            <label for="exampleInputFile">Run</label>
            <select name="run" class="form-control">
                @foreach(\App\Run::take(10)->orderByDesc('start_date')->get() as $run)
                    <option value="{{$run->id}}">{{$run->athlete->formatted_name . " [" . $run->distance . "m] @ " . $run->start_date}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="exampleInputFile">Password</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
        </div>

        <button type="submit" class="btn btn-danger">Remove</button>
    </form>
</div>
</body>
</html>
