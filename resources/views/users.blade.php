<div class="row" style="margin-bottom: 40px;">
    @foreach($athletes->sortBy(function($item){
        return ucwords($item->surname);
    }) as $user)
        <div class="column col-md-4" style="min-height: 185px;">
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <div class="media-object" style="position:relative;width: 150px;height: 150px;border-radius: 4px; background: url({{$user->profile}}); background-size: cover;">
                            @if($user->left_weekly<0)
                            <img src="/medal.png" style="width:70px;position:relative;top:6px;"/>
                            @endif
                        </div>
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">{{$user->formatted_name}}</h4>
                    <h3 style="margin:5px 0;">{{$user->total_distance}}km</h3>
                    <small style="margin:0;" data-toggle="tooltip" data-placement="left" title="@ Current Rate">Projection: {{$user->projected_distance}}km</small>
                    <p style="margin:0;" data-toggle="tooltip" data-placement="left" title="Weekly Target"><small>Weekly Target: </small>{{$user->weekly_target}}km</p>
                    <p style="margin:0;" data-toggle="tooltip" data-placement="left" title="Done This Week"><small>Done This Week: </small>{{$user->weekly_distance}}km</p>
                    <p style="margin:0;" data-toggle="tooltip" data-placement="left" title="{{($user->left_weekly<0?'Bonus':'Left')}} This Week"><small>{{($user->left_weekly<0?'Bonus':'Left')}} This Week: </small>{{($user->left_weekly<0?$user->left_weekly*-1:$user->left_weekly)}}km</p>

                </div>
                <div class="progress" style="width: 100%; margin: 5px 0;">
                    <div class="progress-bar {{(round(($user->weekly_distance/$user->weekly_target)*100,0) == 0? 'progress-bar-danger' :'progress-bar-success')}}" role="progressbar" aria-valuenow="{{round(($user->weekly_distance/$user->weekly_target)*100,0)}}" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width: {{(round(($user->weekly_distance/$user->weekly_target)*100,0) > 100 ? 100 :round(($user->weekly_distance/$user->weekly_target)*100,0)) }}%;">
                        {{round(($user->weekly_distance/$user->weekly_target)*100,0)}}%
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>